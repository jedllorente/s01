# instead of double / , python uses pound (#) sign. 1 liner.
# print is similar to console.log in node/Js since it lets us print messges in the console.
''''''
# although we have no keybind for multi-line comment, it is still possible through the use of 2 sets opf double quotation marks.
# eg: """"""
from pickle import TRUE


# import math - this allows us to use the math module

print("Hello World")


# Variables
# We don'tß have to use keywrods in python to set our variables.
# snake case is the convention in terms of naming the variables in python. it uses underscore(_) in between words and uses lowercasing`
age = 35
middle_initial = "C"
print(age)
print(middle_initial)
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"
print(name1)
print(name2)
print(name3)
print(name4)


# Data TYpes
# string
full_name = "John Doe"
secret_code = "Pa$$word"
# integer/number
num_of_days = 365  # integer
pi_approximation = 3.1416  # float
complex_num = 1+5j  # complex
print(complex_num)
# boolean
is_learning = True
is_difficult = False
print("Hi! My Name is " + full_name)


# F-Strings
# similar to Javascript template literals.
# same as concatenation but acknowledges the data type being held in the variable and converts them to string.
print(f"Hi My name is {full_name}, and my age is {age}.")


# Typecasting - Data type convertion
# is python's way of convereting one data type into another since it does not have type coercion like in Javascript.
# print("My age is " + (age)) :- this would result in an Data Type error
# print("My age is " + str(age)) best practice for mathematical equations.
# from int to string
print("My age is " + str(age))
# from string to int.
print(age + int("9876"))
# integer is different from float in python
print(age + int(98.87))

# Operators
print(2 + 10)  # addition
print(2 - 10)  # subtraction
print(2 * 10)  # multiplication
print(2 / 10)  # division
print(2 % 10)  # remainder
print(2 ** 10)  # exponent
# print(math.sqrt(24)): this is possible if we import built in math module.

# Assignment Operators
num1 = 3
print(num1)
num1 += 4
print(num1)
num1 -= 4
print(num1)
num1 *= 4
print(num1)
num1 /= 4
print(num1)
num1 %= 4
print(num1)


# comparison operators
# returns a boolean value to compare values
print(1 == "1")
print(1 != "1")
print(1 > "1")
print(1 < "1")
print(1 >= "1")
print(1 <= "1")

# Logical operators
print(True and False)  # and
print(True or False)  # or
print(not False)  # not
